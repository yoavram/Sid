[![PyPI](https://img.shields.io/pypi/v/Sid.svg)](https://pypi.python.org/pypi/Sid/)
[![Supported Python versions](https://img.shields.io/pypi/pyversions/Sid.svg)](https://pypi.python.org/pypi/Sid/)
[![License](https://img.shields.io/pypi/l/Sid.svg)](https://github.com/yoavram/Sid/blob/master/LICENCE.txt)
[![Build Status](https://travis-ci.org/yoavram/Sid.svg?branch=master)](https://travis-ci.org/yoavram/Sid)
[![Documentation Status](https://readthedocs.org/projects/sid/badge/?version=latest)](http://sid.readthedocs.org/en/latest/?badge=latest)
[![codecov.io](http://codecov.io/github/yoavram/Sid/coverage.svg?branch=master)](http://codecov.io/github/yoavram/Sid)

# Sid
## Image processing for seed images

![Logo](https://raw.githubusercontent.com/yoavram/Sid/master/Sid.png)

Python script for image processing of plant seed images, specifically _Lamium amplexicaule_.

Read the docs at <http://sid.readthedocs.org/>.